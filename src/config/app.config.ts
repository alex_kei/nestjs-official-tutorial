export default () => ({
  environment: process.env.NODE_ENV || 'development',
  database: {
    host: process.env.DBMS_HOST,
    port: Number(process.env.DBMS_PORT) || 5432,
  }
})