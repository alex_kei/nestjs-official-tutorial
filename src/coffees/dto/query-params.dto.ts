import { IsOptional, IsPositive } from 'class-validator';

export class QueryParamsDto {
  @IsPositive()
  @IsOptional()
  offset: number;

  @IsPositive()
  @IsOptional()
  limit: number;
}
