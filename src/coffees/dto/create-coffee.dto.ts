import { IsNumber, IsString, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCoffeeDto {
  @ApiProperty({ description: 'The name of a coffee' })
  @IsString()
  readonly name: string;

  @ApiProperty({ description: 'The brand of a coffee' })
  @IsString()
  readonly brand: string;

  @ApiProperty({
    description: 'The list of flavors of a coffee',
    example: [],
  })
  @IsString({ each: true })
  readonly flavors: string[];

  @ApiProperty({ description: 'The quantity of a coffee items' })
  @IsNumber()
  @Min(0)
  readonly quantity: number;
};