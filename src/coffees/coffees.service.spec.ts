import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { CoffeesService } from './coffees.service';
import { Coffee, Flavor } from './entities';
import coffeesConfig from './coffees.config';
import { NotFoundException } from '@nestjs/common';

type MockRepository<T = any> = Partial<Record<keyof Repository<T>, jest.Mock>>;
const createMockRepository = <T = any>(): MockRepository<T> => ({
  findOne: jest.fn(),
  create: jest.fn(),
});

describe('CoffeesService', () => {
  let service: CoffeesService;
  let coffeeRepository: MockRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CoffeesService,
        { provide: Connection, useValue: {} },
        { provide: getRepositoryToken(Coffee), useValue: createMockRepository() },
        { provide: getRepositoryToken(Flavor), useValue: createMockRepository() },
        { provide: ConfigService, useValue: { get: () => ({}) } },
        { provide: coffeesConfig.KEY, useValue: {} },
      ],
    }).compile();

    service = module.get<CoffeesService>(CoffeesService);
    coffeeRepository = module.get<MockRepository>(getRepositoryToken(Coffee));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('.findOne', () => {
    describe('When record with provided ID exists', () => {
      it('returns CoffeeEntity object', async () => {
        const coffeeId = 1;
        const expectedResult = {};
        coffeeRepository.findOne.mockReturnValue(expectedResult)
  
        const result = await service.findOne(coffeeId);

        expect(result).toEqual(expectedResult);
      });
    });
    describe('Otherwise', () => {
      it('throws NotFoundException', async () => {
        const coffeeId = 1000;
        coffeeRepository.findOne.mockReturnValue(null);

        try {
          await service.findOne(coffeeId);
        } catch(error) {
          expect(error).toBeInstanceOf(NotFoundException);
          expect(error.message).toEqual(`Record #${coffeeId} not found`);
        }
      });
    });
  });
});
