import {
  Controller,
  Get,
  Post,
  Put,
  Patch,
  Delete,
  Param,
  Query,
  Body,
} from '@nestjs/common';
import { ApiForbiddenResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CoffeesService } from './coffees.service';
import {
  CreateCoffeeDto,
  UpdateCoffeeDto,
  QueryParamsDto,
} from './dto';
import { ParseIntCustomPipe } from '../common/pipe/parse-int.pipe';
import { Public } from '../common/decorators/public.decorator';
import { Protocol } from '../common/decorators/protocol.decorator';

@ApiTags('coffees')
@Controller('coffees')
export class CoffeesController {
  constructor(private readonly service: CoffeesService) {}

  @ApiForbiddenResponse({ description: 'Forbidden' })
  @ApiResponse({ status: 408, description: 'Timeout' })
  @Public()
  @Get()
  async findAll(@Query() queryParams: QueryParamsDto) {
    if (Math.random() > 0.5) {
      await new Promise(resolve => setTimeout(resolve, 4000));
    }
    const { offset, limit } = queryParams;
    console.log({ offset, limit });
    return this.service.findAll(offset, limit);
  }

  @Get(':id')
  findOne(
    @Param('id', ParseIntCustomPipe) id: string,
    @Protocol({ default: 'http' }) protocol: string,
  ) {
    console.log(`Protocol: ${protocol}`);
    return this.service.findOne(Number(id));
  }

  @Post()
  addOne(@Body('product') product: CreateCoffeeDto ): void {
    this.service.addOne(product);
  }

  @Put(':id')
  replaceOne(
    @Param('id') id: string,
    @Body('product') product: CreateCoffeeDto,
  ): void {
    this.service.replaceOne(Number(id), product);
  }

  @Patch(':id')
  updateOne(
    @Param('id') id: string,
    @Body('update') updateData: UpdateCoffeeDto,
  ): void {
    this.service.updateOne(Number(id), updateData);
  }

  @Delete(':id')
  deleteOne(@Param('id') id: string): void {
    this.service.deleteOne(Number(id));
  }

  @Post(':id/recommend')
  async recommendOne(@Param('id') id: string) {
    console.log(id)
    const coffee = await this.service.findOne(Number(id));
    await this.service.recommendCoffee(coffee);
    console.log(coffee);
  }
}
