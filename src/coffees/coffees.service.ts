import {
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ConfigService, ConfigType } from '@nestjs/config';
import { Connection, Repository } from 'typeorm';
import { CreateCoffeeDto, UpdateCoffeeDto } from './dto';
import { Coffee, Flavor, Event } from './entities';
import coffeesConfig from './coffees.config';

@Injectable()
export class CoffeesService {
  constructor(
    @InjectRepository(Coffee)
    private readonly coffeeRepository: Repository<Coffee>,
    @InjectRepository(Flavor)
    private readonly flavorRepository: Repository<Flavor>,
    private readonly connection: Connection,
    private readonly configService: ConfigService,
    @Inject(coffeesConfig.KEY)
    private readonly coffeesConfiguration: ConfigType<typeof coffeesConfig> ,
  ) {
    console.log('Coffee Service instanciated');

    const dataBaseHost = this.configService.get('database.host');
    console.log(`Extracted from config: ${dataBaseHost}`);
    // const dataBaseHostAlt = this.configService.get('database').host;
    // console.log(`Extracted from config (alternative way): ${dataBaseHostAlt}`);
    const dataBaseHostWithDefault = this.configService.get('database.host', 'localhost');
    console.log(`Extracted from config (with default value): ${dataBaseHostWithDefault}`);

    const coffeeConfig = this.configService.get('coffees');
    console.log('Feature config:');
    console.log(coffeeConfig);

    const coffeesTypedConfig = this.coffeesConfiguration;
    console.log('Feature typed config:');
    console.log(coffeesTypedConfig);
    console.log(coffeesTypedConfig.foo);
  }

  findAll(offset = 0, limit = 20): Promise<Coffee[]> {
    return this.coffeeRepository.find({
      skip: offset,
      take: limit,
      relations: ['flavors'],
    });
  }

  async findOne(id: number): Promise<Coffee> {
    const record = await this.coffeeRepository.findOne(
      id,
      { relations: ['flavors'] },
    );
    if(!record) {
      throw new NotFoundException(`Record #${id} not found`);
    }
    return record;
  }

  async addOne(data: CreateCoffeeDto): Promise<unknown> {
    const flavors: Flavor[] = await this.preloadMultipleFlavorsByNames(data.flavors);
    const coffee: Coffee = this.coffeeRepository.create({
      ...data,
      flavors,
    });
    return this.coffeeRepository.save(coffee);
  }

  async updateOne(id: number, patch: UpdateCoffeeDto): Promise<unknown> {
    const coffee: Coffee = await this.coffeeRepository.preload({
      id,
      ...patch,
      ...(patch.flavors &&
        { flavors: await this.preloadMultipleFlavorsByNames(patch.flavors) }
      ),
    });
    return this.coffeeRepository.update(id, coffee);
  }

  async replaceOne(id: number, data: CreateCoffeeDto): Promise<unknown> {
    await this.deleteOne(id);
    return this.addOne(data);
  }

  deleteOne(id: number): Promise<unknown> {
    return this.coffeeRepository.delete(id);
  }

  private async preloadFlavorByName(name: string): Promise<Flavor> {
    const flavor = await this.flavorRepository.findOne({ name });
    if (flavor) {
      return flavor;
    }
    return this.flavorRepository.create({ name });
  }

  private preloadMultipleFlavorsByNames(names: string[]): Promise<Flavor[]> {
    return Promise.all(
      names.map(name => this.preloadFlavorByName(name)),
    );
  }

  async recommendCoffee(coffee: Coffee) {
    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      coffee.recomendations += 1;

      const recomendationEvent = new Event();
      recomendationEvent.name = 'Recommendation Event';
      recomendationEvent.type = 'Coffee-related';
      recomendationEvent.payload = { coffeeId: coffee.id };

      await queryRunner.manager.save(coffee);
      await queryRunner.manager.save(recomendationEvent);

      await queryRunner.commitTransaction();
      console.log(recomendationEvent)
    } catch (error) {
      console.log('Transanction Error:');
      console.log(error);
      await queryRunner.rollbackTransaction();
    } finally {
      queryRunner.release();
    }
  }
}
