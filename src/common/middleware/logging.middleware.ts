import { Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class LoggingMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    console.time('Request-response time');
    console.log('Middleware is entered.');
    console.log('Middleware is exited.');

    res.on('finish', () => console.timeEnd('Request-response time'));
    next();
  }
}
