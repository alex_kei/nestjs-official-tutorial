import {
  createParamDecorator,
  ExecutionContext,
} from '@nestjs/common';

export const Protocol = createParamDecorator(
  (data, context: ExecutionContext) => {
    console.log('Decorator data:');
    console.log(data);
    const defaultValue = typeof data === 'object'
      ? data.default
      : false;
    if (defaultValue) {
      return defaultValue;
    }
    const request = context.switchToHttp().getRequest();
    return request.protocol;
  },
);