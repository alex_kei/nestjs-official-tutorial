import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class WrapResponseInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    console.log('Before intercept...');
    return next.handle()
      .pipe(
        tap(
          data => {
            console.log('After intercept data:');
            console.log(data);
          },
          error => {
            console.log('After intercept error:');
            console.log(error);
          }
        ),
        map(
          data => ({ data }),
        ),
      );
  }
}
