import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Request } from 'express';
import { Reflector } from '@nestjs/core';
import { IS_PUBLIC_KEY } from '../decorators/public.decorator';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class ApiKeyGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly configService: ConfigService,
  ) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    console.log('Guard is entered');
    const isPublic = this.reflector.get(IS_PUBLIC_KEY, context.getHandler());
    if (isPublic) {
      console.log('Guard is exited');
      return true;
    }
    const httpContext = context.switchToHttp();
    const request: Request = httpContext.getRequest<Request>();
    console.log('Guard checks headers list:');
    console.log(request.headers);
    const apiKeyHeader = request.header('Authorization');
    console.log('Guard is exited');
    return apiKeyHeader === this.configService.get('API_KEY');
  }
}
