import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException
} from '@nestjs/common';
import { Response } from 'express';

@Catch()
export class HttpExceptionFilter<T extends HttpException> implements ExceptionFilter {
  catch(exception: T, host: ArgumentsHost) {
    const httpContext = host.switchToHttp();
    const response: Response = httpContext.getResponse<Response>();

    const status = exception.getStatus();
    const exceptionResponse = exception.getResponse();
    const error = typeof exceptionResponse === 'string'
      ? { message: exceptionResponse }
      : (exceptionResponse as object);

    console.log('Log from filter:');
    console.log(error);

    response
      .status(status)
      .json({
        ...error,
        timestamp: (new Date()).toISOString(),
      });
  }
}
