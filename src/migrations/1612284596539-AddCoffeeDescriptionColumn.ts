import {MigrationInterface, QueryRunner} from "typeorm";

export class AddCoffeeDescriptionColumn1612284596539 implements MigrationInterface {
    name = 'AddCoffeeDescriptionColumn1612284596539'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "coffees" ADD "description" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "coffees" DROP COLUMN "description"`);
    }

}
