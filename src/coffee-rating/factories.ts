import { CoffeeDataProvider } from './coffee-rating.provider';

export const simpleFactory = () => [0,1];
export const factoryWithDependacies =
  (dataProvider: CoffeeDataProvider) => dataProvider.create();
export const asyncFactory = async (): Promise<number[]> => {
  console.log('[!] Async Factory');
  const someAsyncData = await Promise.resolve([123, 45678, 90]);
  return someAsyncData;
}