import {
  Controller,
  Get,
} from '@nestjs/common';
import {
  CoffeeTransientService,
  CoffeeRequestService,
} from './coffee-rating.service';

@Controller('coffee-rating-transient')
export class CoffeeRatingTransientController {
  constructor(
    private readonly transientService: CoffeeTransientService,
  ) {}

  @Get()
  findAll() {
    console.log(`Logging: ${this.transientService.greating()}`);
    return this.transientService.greating();
  }
}

@Controller('coffee-rating-request-scoped')
export class CoffeeRatingRequestController {
  constructor(
    private readonly requestService: CoffeeRequestService,
  ) {
    console.log('Coffee Rating request-scoped c0ntroller instantiated')
  }

  @Get()
  findAll() {
    console.log(`Logging: ${this.requestService.greating()}`);
    return this.requestService.greating();
  }
}