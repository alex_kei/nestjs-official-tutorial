import { Module } from '@nestjs/common';
import { CoffeesModule } from '../coffees/coffees.module';
import {
  CoffeeRatingService,
  CoffeeTransientService,
  CoffeeRequestService,
} from './coffee-rating.service';
import {
  COFFEE_ASYNC,
  COFFEE_BRANDS,
  COFFEE_DATA,
  COFFEE_SIMPLE,
  SECOND_INSTANCE,
} from './coffee-rating.constants';
import {
  CoffeeRatingHelper,
  CoffeeRatingTestHelper,
  CoffeeRatingDevHelper,
} from './coffee-rating.helper';
import { CoffeeDataProvider } from './coffee-rating.provider';
import {
  asyncFactory,
  factoryWithDependacies,
  simpleFactory,
} from './factories';
import { DatabaseModule } from '../database/database.module';
import {
  CoffeeRatingTransientController,
  CoffeeRatingRequestController,
} from './coffee-rating.controller';

@Module({
  imports: [
    CoffeesModule,
    DatabaseModule.register({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'pass1234',
    })
  ],
  providers: [
    CoffeeRatingService,
    {
      provide: COFFEE_BRANDS,
      useValue: ['buddy bre', 'nescafe'],
    },
    {
      provide: CoffeeRatingHelper,
      useValue: process.env.TESTING_MODE
        ? CoffeeRatingTestHelper
        : CoffeeRatingDevHelper,
    },
    CoffeeDataProvider,
    {
      provide: COFFEE_DATA,
      useFactory: factoryWithDependacies,
      inject: [CoffeeDataProvider],
    },
    {
      provide: COFFEE_SIMPLE,
      useFactory: simpleFactory,
    },
    {
      provide: COFFEE_ASYNC,
      useFactory: asyncFactory,
    },
    CoffeeTransientService,
    {
      provide: SECOND_INSTANCE,
      useFactory: (transientService: CoffeeTransientService) => transientService.greating(),
      inject: [CoffeeTransientService],
    },
    CoffeeRequestService,
  ],
  controllers: [
    CoffeeRatingTransientController,
    CoffeeRatingRequestController,
  ],
})
export class CoffeeRatingModule {}
