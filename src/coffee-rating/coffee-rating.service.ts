import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { CoffeesService } from '../coffees/coffees.service';
import { COFFEE_ASYNC, COFFEE_BRANDS, COFFEE_DATA, COFFEE_SIMPLE } from './coffee-rating.constants';
import { CoffeeRatingHelper } from './coffee-rating.helper';

@Injectable()
export class CoffeeRatingService {
  constructor(
    private readonly coffeesService: CoffeesService,
    @Inject(COFFEE_BRANDS)
    private readonly coffeeBrands: string[],
    private readonly helper: CoffeeRatingHelper,
    @Inject(COFFEE_DATA)
    private readonly data: number[],
    @Inject(COFFEE_SIMPLE)
    private readonly simpleData: number[],
    @Inject(COFFEE_ASYNC)
    private readonly asyncData: number[],
  ) {
    console.log(`Brands: ${coffeeBrands}`);
    console.log(`Environment: ${process.env.TESTING_MODE}`);
    console.log(helper);
    console.log(`Provided Data: ${data}`);
    console.log(`Simple Data: ${simpleData}`);
    console.log(`Async Data: ${asyncData}`);
  }
}

@Injectable({ scope: Scope.TRANSIENT })
export class CoffeeTransientService {
  constructor() {
    console.log('Transient Service instantiated');
  }

  greating() {
    return 'Hello';
  }
}

@Injectable({ scope: Scope.REQUEST })
export class CoffeeRequestService {
  constructor(
    @Inject(REQUEST)
    private readonly request: Request,
  ) {
    console.log('Request-scoped Service instantiated');
    console.log(request.hostname);
  }

  greating() {
    return 'Hello';
  }
}
